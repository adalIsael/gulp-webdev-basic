//1. definir tarea para minificar imagenes con el pluggin gulp-imagemin
//2. definir tarea para minificar imagenes con el pluggin gulp-clean-css, gulp-autoprefixer
//3. definir tarea para minificar imagenes con el pluggin gulp-htmlmin
//4. definir tarea para minificar imagenes con el pluggin gulp-uglify

var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var cleancss = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify');


gulp.task('minificar-imagenes', function() {
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/images'));
});

gulp.task('minificar-css', function() {
    gulp.src('src/css/*')
        .pipe(cleancss())
        .pipe(autoprefixer())
        .pipe(gulp.dest('build/css'))
});

gulp.task('minificar-html', function() {
    gulp.src('src/html/*')
        .pipe(htmlmin())
        .pipe(gulp.dest('build/'));
});

gulp.task('minificar-uglify', function() {
    gulp.src('src/js/*')
        .pipe(uglify())
        .pipe(gulp.dest('build/js'));
});
